# Sentry測試
使用Docker進行異常通知測試，

**OCP上測試可以[參考說明](README_OCP.md)**

## 環境設定

### Step1 設定必要的環境變數
開啟 **.env**檔案，設定對應的smtp設定
```
SENTRY_SECRET_KEY=967z5x5xoenz9ui#+%%1hk3ot4*7%lh2slbo_zzqhki8kr-%*d
# Mail Service
SENTRY_EMAIL_HOST=smtp.gmail.com
SENTRY_EMAIL_USER=???
SENTRY_SERVER_EMAIL=???
# auth
SENTRY_EMAIL_PASSWORD=???
SENTRY_EMAIL_USE_TLS=true
SENTRY_EMAIL_PORT=587
```

** P.S. 如果要重新產生KEY可以使用此指令 `docker run --rm sentry config generate-secret-key` **


### Step2 啟動其它必要的Container

#### Redis
```
docker run \
  --detach \
  --name sentry-redis \
  redis:3.2-alpine
```

#### DB (postgres)
```
docker run \
  --detach \
  --name sentry-postgres \
  --env POSTGRES_PASSWORD=secret \
  --env POSTGRES_USER=sentry \
  postgres:9.5
```

### Step3 初始化Sentry，同時建立Admin帳號
使用此一指令取得的key建立新的服務，並且在最後會示建立admin的user/password
```
docker run -it --rm \
--env-file .env \
--link sentry-postgres:postgres \
--link sentry-redis:redis \
sentry upgrade
```

### Step4 啟動Sentry
包含主要的服務，以及背景的Worker & Cron兩個服務
```
docker run -d --name sentry-server \
-p 9000:9000 \
--env-file .env \
--link sentry-postgres:postgres \
--link sentry-redis:redis \
sentry

docker run -d --name sentry-cron \
--env-file .env \
--link sentry-postgres:postgres \
--link sentry-redis:redis \
sentry run cron


docker run -d --name sentry-worker-1 \
--env-file .env \
--link sentry-postgres:postgres \
--link sentry-redis:redis \
sentry run worker
``` 

## SpringBoot測試
### Step1 取得DSN Key
1. Login Sentry
1. 進入 **Setting->Projects->Create Project**
1. 建立 Java Project，並拉到最下面點選 **Got it!**
1. 進入 ** Settings->sentry->{ProjectName}->General->ClientKeys
1. 取得DSN

### Step2 啟動SpringBoot測試專案
1. git clone https://github.com/sentry-demos/java-spring-boot-log4j
2. 開啟 sentry.properties，並放入Step1取得的DSN
3. 啟動 spring-boot:run

### Step3 測試
以用以下網址測試，又因為同一種Exception不會一直通知，所以用過了就用其它的
- http://127.0.0.1:8080/unhandled
- http://127.0.0.1:8080/handled
- http://127.0.0.1:8080/capture-message
- http://127.0.0.1:8080/checkout


## 已準備好的環境重新啟動
```
docker start sentry-redis
docker start sentry-postgres
docker start sentry-server
docker start sentry-cron
docker start sentry-worker-1
```

## 其它參考資料
- https://michael728.github.io/2019/08/11/java-spring-boot-sentry-log4j2/index.html
- https://hub.docker.com/_/sentry/