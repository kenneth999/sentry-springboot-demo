# 於OCP上使用Sentry
使用OCP進行異常通知測試，

## 環境設定

### Step1 設定必要的環境變數
於ConfigMap新增以下設定
```
data:
  SENTRY_SECRET_KEY: '967z5x5xoenz9ui#+%%1hk3ot4*7%lh2slbo_zzqhki8kr-%*d'
  SENTRY_EMAIL_HOST: smtp.gmail.com
  SENTRY_EMAIL_USER: ???
  SENTRY_SERVER_EMAIL: ???
  SENTRY_EMAIL_USE_TLS: 'true'
  SENTRY_EMAIL_PORT: '587'
  
  SENTRY_POSTGRES_HOST: postgres
  SENTRY_POSTGRES_PORT: '5432'
  SENTRY_DB_NAME: postgres
  SENTRY_DB_USER: postgres
  SENTRY_DB_PASSWORD: postgres
```

### Step2 建必必要Container

#### Redis
使用OCP介面新增
- OCP UI: Add->From Catalog
- Item Name: Redis (Ephemeral)
- Service Name: redis

#### Postgres 9.5
使用OCP介面新增
- OCP UI: Add->Container Image
- Docker Image: registry.access.redhat.com/rhscl/postgresql-95-rhel7
- Service Name: postgres

### Step3 初始化Sentry，同時建立Admin帳號
1. 使用OCP介面新增Sentry服務
- OCP UI: Add->Container Image
- Docker Image: docker.io/sentry
- Service Name: sentry
2. 開啟 Workloads->Deployment Configs->sentry
3. 開啟YAML
  - spec->strategy->rollingParams-> timeoutSeconds 值由600改為1200
  - 並新增以下內容到 spec->template->spec->containers 位置，用以讓服務進行初始化
```
          command:
            - "/bin/bash"
            - "-ecx"
            - |
              # Init migrations
              sentry upgrade --noinput
              # Create user to sentry
              sentry createuser --email sentra@ocp.usr --password "sentry" --superuser --no-input
```
4. 開啟ENVIRONMENT
  - Add SENTRY_EMAIL_PASSWORD = ？？？(可以由Secret Param設定後指定)
  - Add ConfigMap "sentryCfg"

5. 當上設設定的POD已完成執行後，再將上述設定移除

**注意：建立時必須同時建立Route**

### Step4 啟動Sentry
1. 使用OCP介面新增Sentry服務
- OCP UI: Add->Container Image
- Docker Image: docker.io/sentry
- Service Name: sentry-worker 及 sentry-cron 兩個
2. 開啟 Workloads->Deployment Configs->sentry
3. 開啟YAML
  - worker新增以下內容到 spec->template->spec->containers 位置，用以讓服務進行初始化
```
          command:
            - "/bin/bash"
            - "-ecx"
            - |
              sentry run worker
```
  - cron新增以下內容到 spec->template->spec->containers 位置，用以讓服務進行初始化
```
          command:
            - "/bin/bash"
            - "-ecx"
            - |
              sentry run cron
```
4. 開啟ENVIRONMENT
  - Add C_FORCE_ROOT = "true"
  - Add SENTRY_EMAIL_PASSWORD = ？？？(可以由Secret Param設定後指定)
  - Add ConfigMap "sentryCfg"

## SpringBoot測試
### Step1 取得DSN Key
1. Login Sentry
1. 進入 **Setting->Projects->Create Project**
1. 建立 Java Project，並拉到最下面點選 **Got it!**
1. 進入 ** Settings->sentry->{ProjectName}->General->ClientKeys
1. 取得DSN

### Step2 啟動SpringBoot測試專案
1. 使用OCP介面新增Sentry服務
- OCP UI: Add-> From Git
- Git Repo URL: https://github.com/sentry-demos/java-spring-boot-log4j
- Service Name: sentry-test-service

### Step3 設定sentry連結
1. 開啟 Workloads->Deployment Configs->sentry-test-service
1. 於ENVIRONMENT新增以下變數(值是Step1取得)
SENTRY_DSN=http://121cd00ff49741cc94cc93a937d3c28d@sentry-sentry.apps.ocp4.com/2"

### Step4 測試
**將下方的IP:PORT改為Step2取得的Route URL進行測試**，
需注意的是同一種Exception不會一直通知，所以用過了就用其它的
- http://127.0.0.1:8080/unhandled
- http://127.0.0.1:8080/handled
- http://127.0.0.1:8080/capture-message
- http://127.0.0.1:8080/checkout

**注意：建立時必須同時建立Route**

## 其它參考資料
- https://docs.sentry.io/clients/java/
- https://github.com/thoth-station/sentry-openshift/tree/master/openshift
